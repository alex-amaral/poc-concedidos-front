// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  envName: 'local',
  keycloak: {
    // Url of the Identity Provider
    issuer: 'auth',

    // Realm
    realm: 'concedidosRealm',

    // The SPA's id. 
    // The SPA is registerd with this id at the auth-serverß
    clientId: 'springboot-microservice',
    clientSecret: "3ea94323-c6a9-4c92-bc93-f9fe466b889c"
  },
  concedidosApi: {
    url: '/api/'
  }
  
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
