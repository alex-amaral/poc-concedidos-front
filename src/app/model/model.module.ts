import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";

import { SolicitacaoRepository } from "./solicitacao.repository";
import { StaticDataSource } from "./static.datasource";
import { RestDataSource } from "./rest.datasource";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  declarations: [],
  providers: [
    SolicitacaoRepository, 
    { provide: StaticDataSource, useClass: RestDataSource } 
  ]
})
export class ModelModule { }
