import { Injectable } from "@angular/core";
import { Solicitacao } from "./solicitacao.model";
import { Observable, from } from "rxjs";

@Injectable()
export class StaticDataSource {

    private solicitacoes: Solicitacao[] = [
        new Solicitacao("100000-001", "Jose das Couves", "Liberado", 50000),
        new Solicitacao("500000-000", "Maria Antonieta", "Em analise",  70000),
        new Solicitacao("680000-010", "Josefina de Almeida", "Negado", 1000000),
        new Solicitacao("200000-001", "Octavia Franca", "Liberado", 50000),
        new Solicitacao("300000-000", "Oswaldo de Souza", "Em analise",  70000),
        new Solicitacao("480000-010", "Carlos de Almeida", "Negado", 1000000),
        new Solicitacao("500000-001", "Jose Pedro das Couves", "Liberado", 50000),
        new Solicitacao("650000-000", "Helena Pereira Amaral", "Em analise",  70000),
        new Solicitacao("780000-010", "Cristina Pereira", "Negado", 1000000),
        new Solicitacao("800000-001", "Zilda das Couves", "Liberado", 50000),
        new Solicitacao("900000-000", "Antonieta de Castro", "Em analise",  70000),
        new Solicitacao("100000-010", "Jiselda Carla Almeida", "Negado", 1000000),
        new Solicitacao("110000-001", "Pedro Jose Mediros", "Liberado", 50000),
        new Solicitacao("120000-000", "Ana Maria  Antonieta", "Em analise",  70000),
        new Solicitacao("130000-010", "Pedro algusto de Almeida", "Negado", 1000000),
        new Solicitacao("150000-001", "Antonio das Couves", "Liberado", 50000),
        new Solicitacao("560000-000", "Maria Joaquina Santos", "Em analise",  70000),
        new Solicitacao("690000-010", "Jesus  dos Santos", "Negado", 1000000),
        new Solicitacao("166000-001", "Marco Antonio das Couves", "Liberado", 50000),
        new Solicitacao("598660-000", "Flor Antonieta", "Em analise",  70000),
        new Solicitacao("680001-010", "Adelia de Almeida", "Negado", 1000000)
    ];

  getSolicitacoes(): Observable<Solicitacao[]> {
    return from([this.solicitacoes]);
  }
}

