import { Injectable } from "@angular/core";
import { StaticDataSource } from "./static.datasource";
import { Solicitacao } from "./solicitacao.model";

@Injectable()
export class SolicitacaoRepository {
    private solicitacoes: Solicitacao[] = [];
    private situacoes: string[] = [];
  
    constructor(private dataSource: StaticDataSource) {
      dataSource.getSolicitacoes().subscribe((data) => {
        this.solicitacoes = data;
        this.situacoes = data
          .map((s) => s.situacao)
          .filter((c, index, array) => array.indexOf(c) == index)
          .sort();
      });
    }
  
    getSolicitacoes(situacao: string = null): Solicitacao[] {      
      return this.solicitacoes.filter(
        (s) => situacao == null || situacao == s.situacao
      );
    }
  
    getSolicitacao(matricula: string): Solicitacao {
      return this.solicitacoes.find((s) => s.matricula == matricula);
    }
  
    getSituacoes(): string[] {
      return this.situacoes;
    }
}