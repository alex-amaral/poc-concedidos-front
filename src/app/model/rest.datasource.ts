import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Solicitacao } from "./solicitacao.model";

import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class RestDataSource {
  baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = environment.concedidosApi.url;
  }

  getSolicitacoes(): Observable<Solicitacao[]> {
    console.log("Recuperar solicitacoes backend")
    console.log("BaseUrl: "+ this.baseUrl )
    return this.http.get<Solicitacao[]>(this.baseUrl + "solicitacao")
  }
}
