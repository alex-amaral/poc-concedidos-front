export class Solicitacao {
    constructor(
        public matricula?: string,
        public beneficiario?: string,
        public situacao?: string,
        public valor?: number) { 
    }
}