import { BrowserModule } from "@angular/platform-browser";
import { NgModule, APP_INITIALIZER } from "@angular/core";

import { AppComponent } from "./app.component";
import { ConsultaGeralModule } from "./consulta-geral/consulta-geral.module";

import { LOCALE_ID } from "@angular/core";
import br from "@angular/common/locales/pt";
import { registerLocaleData } from "@angular/common";

import { KeycloakService, KeycloakAngularModule } from "keycloak-angular";
import { initializer } from "../app/utils/app-init";

registerLocaleData(br, "pt");

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, KeycloakAngularModule, ConsultaGeralModule],
  providers: [
    { provide: LOCALE_ID, useValue: "pt" },
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      deps: [KeycloakService],
      multi: true,
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
