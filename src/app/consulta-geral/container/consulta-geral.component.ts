import { Component, OnInit } from '@angular/core';
import { Solicitacao } from "../../model/solicitacao.model";
import { SolicitacaoRepository } from "../../model/solicitacao.repository";

@Component({
  selector: 'app-consulta-geral',
  templateUrl: './consulta-geral.component.html',
  styleUrls: ['./consulta-geral.component.css']
})
export class ConsultaGeralComponent implements OnInit {
  public solicitacoesPerPage = 5;
  public selectedPage = 1;

  constructor(private repository: SolicitacaoRepository) { }

  get solicitacoes(): Solicitacao[] {
    let pageIndex = (this.selectedPage - 1) * this.solicitacoesPerPage;
    return this.repository
      .getSolicitacoes()
      .slice(pageIndex, pageIndex + this.solicitacoesPerPage);
  }

  get situacoes(): string[] {
    return this.repository.getSituacoes();
  }

  changePage(newPage: number) {
    this.selectedPage = newPage;
  }

  changePageSize(newSize: number) {
    this.solicitacoesPerPage = Number(newSize);
    this.changePage(1);
  }

  get pageCount(): number {
    return Math.ceil(this.repository
      .getSolicitacoes().length / this.solicitacoesPerPage)
  }

  ngOnInit() {
  }
}
