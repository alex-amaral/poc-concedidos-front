import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsultaGeralComponent } from './container/consulta-geral.component';
import { ModelModule } from "../model/model.module";
import { CounterDirective } from "./component/counter.directive";

@NgModule({
  imports: [
    CommonModule,
    ModelModule
  ],
  declarations: [
    ConsultaGeralComponent,
    CounterDirective
  ],
  exports: [
    ConsultaGeralComponent
  ]
})
export class ConsultaGeralModule { }
